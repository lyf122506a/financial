package com.financial.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.financial.entity.enums.OrderStatus;
import com.financial.entity.enums.OrderType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "order_t")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Order {

    @Id
    private String orderId;

    //渠道id
    private String chanId;

    private String chanUserId;

    /**
     * @see OrderType
     */
    private String orderType;

    private String productId;

    private BigDecimal amount;

    private String outerOrderId;

    /**
     * @see OrderStatus
     */
    private String orderStatus;

    private String memo;

    @JsonFormat(pattern = "YYYY-MM-DD HH:mm:ss")
    private Date createAt;

    @JsonFormat(pattern = "YYYY-MM-DD HH:mm:ss")
    private Date updateAt;
}
