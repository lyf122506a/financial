package com.financial.api;

import com.financial.api.domain.ProductRpcReq;
import com.financial.entity.Product;
import com.googlecode.jsonrpc4j.JsonRpcService;

import java.util.List;


@JsonRpcService("rpc/products")
public interface ProductRPC {

    /**
     * 查询多个产品
     *
     * @param productRpcReq
     * @return
     */
   List<Product> query(ProductRpcReq productRpcReq);

    /**
     * 查询单个个产品
     * @param id
     * @return
     */
   Product findOne(String id);
}
