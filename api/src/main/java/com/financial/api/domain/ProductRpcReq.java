package com.financial.api.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

/**
 * 产品相关rpc请求参数
 */
@ToString
@Getter
@Setter
public class ProductRpcReq {


    private List<String> idList;
    private BigDecimal minRewardRate;
    private BigDecimal maxRewardRate;
    private List<String> statusList;




}
