package com.financial.seller.configuration;

import com.financial.api.ProductRPC;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcClientProxyCreator;

import java.net.MalformedURLException;
import java.net.URL;


@Configuration
@Slf4j
@ComponentScan(basePackageClasses = {ProductRPC.class})
public class RpcConfiguration {

    @Value("${rpc.client.url}")
    private String aaa;



    @Bean
    public AutoJsonRpcClientProxyCreator autoJsonRpcClientProxyCreator(@Value("http://localhost:8081/manager/")String url){
        AutoJsonRpcClientProxyCreator autoJsonRpcClientProxyCreator=new AutoJsonRpcClientProxyCreator();
        try {
            autoJsonRpcClientProxyCreator.setBaseUrl(new URL(url));
            log.info("创建rpc服务地址",url);
        } catch (MalformedURLException e) {
            log.error("创建rpc服务地址错误",e);
        }
        autoJsonRpcClientProxyCreator.setScanPackage(ProductRPC.class.getPackage().getName());
        return autoJsonRpcClientProxyCreator;
    }
}
