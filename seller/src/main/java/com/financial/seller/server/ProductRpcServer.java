package com.financial.seller.server;

import com.financial.api.ProductRPC;

import com.financial.api.domain.ProductRpcReq;
import com.financial.api.events.ProductStatusEvent;
import com.financial.entity.Product;

import com.financial.entity.enums.ProductStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ProductRpcServer implements ApplicationListener{

    @Autowired
    private ProductRPC productRPC;
    @Autowired
    private ProductCache productCache;

    static final String MQ_DESTINATION = "Consumer.cache.VirtualTopic.PRODUCT_STATUS";

    /***
     * 查询所有产品
     * @return
     */
    public List<Product> findAll(){

        return productCache.readAllCache();
    }

    public Product findOne(String id){
        log.debug("查询单个产品，id={}", id);
        //Assert.notNull(id, "需要产品编号参数");
        Product product = productCache.readCache(id);
        log.debug("创建产品, 结果:{}", product);
        if (product==null){
            productCache.removeCache(id);
        }
        return product;
    }


    //@PostConstruct
    public void init() {
        System.out.print(findAll().toString());
        System.out.print(findOne("001").toString());
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        List<Product> products = findAll();
        products.forEach(product -> {
            productCache.putCache(product);
        });
    }

    @JmsListener(destination = MQ_DESTINATION)
    void updateCache(ProductStatusEvent event) {
        log.info("receive event:{}", event);
        productCache.removeCache(event.getId());
        if (ProductStatus.IN_SELL.equals(event.getStatus())) {
            productCache.readCache(event.getId());
        }
    }


}
