package com.financial.seller.server;


import com.financial.api.ProductRPC;
import com.financial.api.domain.ProductRpcReq;
import com.financial.entity.Product;
import com.financial.entity.enums.ProductStatus;
import com.hazelcast.core.HazelcastInstance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 产品缓存
 */
@Component
@Slf4j
public class ProductCache {

    static final String CACHE_NAME = "imooc_product";


    @Autowired
    private ProductRPC productRpc;
    @Autowired
    private HazelcastInstance hazelcastInstance;


    /**
     * 读取缓存
     * @param id
     * @return
     */
    @Cacheable(cacheNames = CACHE_NAME)
    public Product readCache(String id){
        log.info("rpc查询单个产品,请求:{}", id);
        Product result = productRpc.findOne(id);
        log.info("rpc查询单个产品,结果:{}", result);
        return result;
    }


    @CachePut(cacheNames = CACHE_NAME,key = "#product.id")
    public Product putCache(Product product){
        return product;
    }

    @CacheEvict(cacheNames = CACHE_NAME)
    public void removeCache(String id){

    }

    public List<Product> readAllCache(){
        Map map = hazelcastInstance.getMap(CACHE_NAME);
        List<Product> products = null;
        if(map.size() >0 ){
            products = new ArrayList<>();
            products.addAll(map.values());
        }else {
            products = findAll();
        }
        return products;
    }


    /***
     * 查询所有产品
     * @return
     */
    public List<Product> findAll(){
        ProductRpcReq req=new ProductRpcReq();
        List<String> stauts=new ArrayList<>();
        stauts.add(ProductStatus.IN_SELL.name());
        req.setStatusList(stauts);
        log.info("rpc查询全部产品,请求:{}", req);
        List<Product> result = productRpc.query(req);
        log.info("rpc查询全部产品,结果:{}", result);
        return result;
    }

}
