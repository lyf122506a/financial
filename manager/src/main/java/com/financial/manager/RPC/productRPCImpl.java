package com.financial.manager.RPC;

import com.financial.entity.Product;
import com.financial.api.ProductRPC;
import com.financial.api.domain.ProductRpcReq;
import com.financial.manager.service.ProductService;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * rpc服务实现类
 */
@AutoJsonRpcServiceImpl
@Service
@Slf4j
public class productRPCImpl implements ProductRPC {
    @Autowired
    private ProductService productService;

    @Override
    public List<Product> query(ProductRpcReq productRpcReq) {
        log.info("查询多个产品,请求:{}", productRpcReq);
        Pageable pageable = new PageRequest(0,
                                            1000,
                                            Sort.Direction.DESC,
                                            "rewardRate");
        Page<Product> result = productService.query(productRpcReq.getIdList(),
                                                    productRpcReq.getMinRewardRate(),
                                                    productRpcReq.getMaxRewardRate(),
                                                    productRpcReq.getStatusList(),
                                                    pageable);
        log.info("查询多个产品,结果:{}", result);
        return result.getContent();
    }

    @Override
    public Product findOne(String id) {
        log.info("查询产品详情,请求:{}", id);
        Product result = productService.findOne(id);
        log.info("查询产品详情,结果:{}", result);
        return result;
    }
}
