package com.financial.manager.service;

import com.financial.api.events.ProductStatusEvent;
import com.financial.entity.enums.ProductStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 管理产品状态
 */
@Component
@Slf4j
public class ProductStatusManager {
    static  final String MQ_DESTINATION = "VirtualTopic.PRODUCT_STATUS";


    @Autowired
    private JmsTemplate jmsTemplate;

    public void changeStatus(String id, ProductStatus status){
        ProductStatusEvent event = new ProductStatusEvent(id,status);
        log.info("send message:{}",event);
        jmsTemplate.convertAndSend(MQ_DESTINATION,event);
    }


    @PostConstruct
    public void init(){
        changeStatus("001",ProductStatus.FINISHED);
    }
}
