package com.financial.manager;

import com.swagger.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@EntityScan(basePackages = {"com.financial.entity"})//扫描DSC
@Import(SwaggerConfiguration.class)
public class ManagerApp {
    public static void main(String[] arg){
        SpringApplication.run(ManagerApp.class);
    }
}
